package at.pif.games.SpaceInvaders;

import org.newdawn.slick.*;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class Player implements Actor {
    private Image rocketImage;
    private float x, y;
    private int speed;

    public Player() throws SlickException {
        Image tmp = new Image("at/pif/games/images/Rocket1.png");
        this.rocketImage = tmp.getScaledCopy(50,50);
        this.x = 100;
        this.y = 100;
        this.speed = 100;

    }

    @Override
    public void render(Graphics graphics) {
        rocketImage.draw(this.x, this.y);
        Shape PlayerHitbox = new Rectangle(this.x, this.y, 50, 50);
        graphics.draw(PlayerHitbox);
    }


    @Override
    public void update(GameContainer gameContainer, int delta) {

        if (gameContainer.getInput().isKeyDown(Input.KEY_RIGHT)) {
            this.x++;
            this.x += (float)delta/this.speed;
        }
        if (gameContainer.getInput().isKeyDown(Input.KEY_LEFT)) {
            this.x--;
            this.x -= (float)delta/this.speed;
        }
        if (gameContainer.getInput().isKeyDown(Input.KEY_UP)) {
            this.y--;
            this.y -= (float)delta/this.speed;
        }
        if (gameContainer.getInput().isKeyDown(Input.KEY_DOWN)) {
            this.y++;
            this.y += (float)delta/this.speed;
        }
        if (this.x > 750){
            this.x = 750;
        }

        if(this.x < 0){
            this.x = 0;
        }

        if (this.y < 350){
            this.y = 350;
        }

        if(this.y > 550){
            this.y = 550;
        }
    }


    public float getX() {
        return x + 21;
    }

    public float getY() {
        return y;
    }
}