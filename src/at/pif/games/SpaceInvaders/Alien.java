package at.pif.games.SpaceInvaders;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class Alien implements Actor {
    private Image alienImage;
    private float x, y;

    public Alien() throws SlickException {
        Image tmp = new Image("at/pif/games/images/Alien1.png");
        this.alienImage = tmp.getScaledCopy(50,50);
        this.x = 100;
        this.y = 100;


    }
    @Override
    public void render(Graphics graphics) {
        alienImage.draw(this.x, this.y);
        Shape AlienHitbox = new Rectangle(this.x, this.y, 50, 50);
        graphics.draw(AlienHitbox);
    }

    @Override
    public void update(GameContainer gameContainer, int delta) {

    }
}
