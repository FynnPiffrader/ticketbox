package at.pif.games.SpaceInvaders;

import org.newdawn.slick.*;

import java.util.ArrayList;
import java.util.List;

public class SpaceInvaders extends BasicGame {
    private List<Actor> actors;
    private Player rocket;
    private Alien alien;
    public SpaceInvaders(String title) {
        super(title);
    }

    @Override
    public void init(GameContainer gameContainer) throws SlickException {
        this.actors = new ArrayList<>();
        this.actors = new ArrayList<>();

        Player rocket = new Player();
        this.rocket = rocket;
        this.actors.add(rocket);

        Alien alien = new Alien();
        this.alien = alien;
        this.actors.add(alien);

    }

    @Override
    public void update(GameContainer gameContainer, int delta) throws SlickException {
        for (Actor actor:this.actors) {
            actor.update(gameContainer, delta);
        }
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        for (Actor actor:this.actors) {
            actor.render(graphics);
        }
    }

    @Override
    public void keyPressed(int key, char c) {
        if (key == Input.KEY_SPACE) {
            System.out.println("shoot");
            Cannonball cb = new Cannonball(this.rocket.getX(), this.rocket.getY());
            this.actors.add(cb);
        }
    }

    public static void main(String[] argv) {
        try {
            AppGameContainer container = new AppGameContainer(new SpaceInvaders("Epic SpaceInvaders"));
            container.setDisplayMode(800,600,false);
            container.setVSync(true);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }

    }
}
