package at.pif.games.firstgame;

import org.newdawn.slick.*;

public class Rectangle extends BasicGame {
    private float xRec;
    private float yRec;
    private float xOval;
    private float yOval;
    private float xCircle;
    private float yCircle;
    private float speed;
    private float director;

    int speedRec = 1;
    int speedCircle = 1;
    int speedOval = 1;


    public Rectangle(String title) {
        super(title);
    }

    @Override
    public void init(GameContainer gameContainer) throws SlickException {
        this.speed = 2.0f;
        this.xRec = 100;
        this.yRec = 100;
        this.xOval = 0;
        this.yOval = 0;
        this.xCircle = 0;
        this.yCircle = 50;
        this.director = 1;
    }

    @Override
    public void update(GameContainer gameContainer, int delta) throws SlickException {

//RECTANGLE

        if (speedRec == 1){
            this.xRec += (float)delta/this.speed;
            if (this.xRec >= 600) {
                this.xRec += (float) delta * 0;
                this.xRec = 600;
                this.speedRec = 2;
            }
        }
        if (speedRec == 2) {
            this.yRec += (float) delta / speed;
            if (this.yRec >= 450) {
                this.yRec += (float) delta * 0;
                this.yRec = 450;
                this.speedRec = 3;
            }
        }
        if (speedRec == 3) {
            this.xRec -= (float) delta / speed;
            if (this.xRec <= 100) {
                this.xRec += (float) delta * 0;
                this.xRec = 100;
                this.speedRec = 4;
            }
        }

        if (speedRec == 4) {
            this.yRec -= (float) delta / speed;
            if (this.yRec <= 100) {
                this.yRec += (float) delta * 0;
                this.yRec = 100;
                this.speedRec = 1;
            }
        }

//circle

        if(speedCircle == 1){
            this.yCircle += (float) delta / speed;
            if(this.yCircle>=500){
                this.yCircle += (float) delta * 0;
                this.yCircle = 500;
                this.speedCircle = 2;
            }
        }

        if(speedCircle == 2){
            this.yCircle -= (float) delta / speed;
            if (this.yCircle <= 100){
                 this.yCircle += (float) delta * 0;
                 this.yCircle = 100;
                 this.speedCircle = 1;
            }
        }
//oval
        if(speedOval == 1){
            this.xOval += (float) delta / speed;
            if(this.xOval >= 600){
                this.xOval += (float) delta * 0;
                this.xOval = 600;
                this.speedOval = 2;
            }
        }

        if(speedOval == 2){
            this.xOval -= (float) delta / speed;
            if(this.xOval <= 100){
                this.xOval += (float) delta * 0;
                this.xOval = 100;
                this.speedOval = 1;
            }
        }

    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        graphics.drawRect(this.xRec,this.yRec,100,100);
        graphics.drawString("Hello", 100, 90);
        graphics.drawOval(this.xOval,this.yOval,100,50);
        graphics.drawOval(this.xCircle,this.yCircle,50,50);

    }
    public static void main(String[] argv) {
        try {
            AppGameContainer container = new AppGameContainer(new Rectangle("Rectangle"));
            container.setDisplayMode(800,600,false);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}