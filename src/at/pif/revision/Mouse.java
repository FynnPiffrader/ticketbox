package at.pif.revision;

public class Mouse {
    private String color;
    private int weight;
    private Battery battery;
    private int life;

    public Mouse(int weight){
        this.weight = weight;
        this.color = "blue";
        this.life = 10;
    }

    public void leftButtonClicked(){
        System.out.println("My Color is -" + color + "- i agree");
        this.life -= 1;
        double actualCurrent = this.battery.getCurrent();
        actualCurrent -= 0.1;
        battery.setCurrent(actualCurrent);
    }

    public void spamRightClicke(){
        System.out.println("pleas stop spamming onii-chan");
        double actualCurrent = this.battery.getCurrent();
        actualCurrent -= 1;
        battery.setCurrent(actualCurrent);
        this.life -= 10;
        if (this.life <= 10){
            System.out.println("i am a dead mouse");
        }
    }

    public Battery getBattery() {
        return battery;
    }

    public void setBattery(Battery battery) {
        this.battery = battery;
    }
}
