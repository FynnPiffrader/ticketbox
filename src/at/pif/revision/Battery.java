package at.pif.revision;

public class Battery {
    private double current;
    private String color;

    public Battery(double current) {
        this.current = current;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getCurrent() {
        return current;
    }

    public void setCurrent(double current) {
        this.current = current;
    }
}
