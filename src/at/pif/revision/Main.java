package at.pif.revision;

public class Main {
    public static void main (String[] args){
        Mouse m1 = new Mouse(200);
        Mouse m2 = new Mouse(200);
        Battery b1 = new Battery(2.6);
        Battery b2 = new Battery(8.3);

        m1.setBattery(b1);
        m2.setBattery(b2);
        b1.setColor("white");
        m1.leftButtonClicked();
        m2.spamRightClicke();

        System.out.println(m2.getBattery().getCurrent());
    }
}
