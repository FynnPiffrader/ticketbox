package at.pif.mobilephone;

public class Camera {
    private int resolution;
    private int fileNumber = 0;

    public Camera(int resolution) {
        this.resolution = resolution;
    }

    public PhoneFile makePicture(){
        PhoneFile file = new PhoneFile("png", 500, "pic" + fileNumber);
        fileNumber++;
        return file;
    }
}
