package at.pif.mobilephone;

import java.util.List;

public class Phone {
    private String color;
    private SIM sim;
    private Camera camera;
    private SDCard sdCard;

    public Phone(String color, SIM sim, Camera camera, SDCard sdCard) {
        this.color = color;
        this.sim = sim;
        this.camera = camera;
        this.sdCard = sdCard;
    }

    public void makeCall(String number){
        this.sim.doCall(number);
    }
    public void takePicture(){
        PhoneFile file = this.camera.makePicture();
        this.sdCard.save(file);
    }

    public void printAllFiles(){
        List<PhoneFile> files = this.sdCard.getFiles();
        for (PhoneFile file : files){
            System.out.println(file.getInfo());
        }
    }

    public int getFreeSpace(){
        return this.sdCard.getFreeSpace();
    }
}
