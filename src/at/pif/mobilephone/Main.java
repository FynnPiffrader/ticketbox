package at.pif.mobilephone;

public class Main {
    public static void main(String[] args) {
        SIM s1 = new SIM(1, "1234");
        Camera c1 = new Camera(123);
        SDCard sd1 = new SDCard(10000);
        Phone p1 = new Phone("green", s1, c1, sd1);

        p1.makeCall("1111");

        p1.takePicture();
        p1.takePicture();

        p1.printAllFiles();

        System.out.println(p1.getFreeSpace());
        p1.takePicture();
        System.out.println(p1.getFreeSpace());
    }
}
