package at.pif.ticketautomat;

import java.util.Date;

public class Ticket {
    private int id;
    private long ats;
    private long bts;

    public Ticket() {
        this.id = 1;
        this.ats = (new Date()).getTime();
        this.bts = (new Date()).getTime();
    }


    public int getId() {
        return id;
    }

    public long getAts() {
        return ats;
    }

    public long getBts() {
        return bts;
    }
}
