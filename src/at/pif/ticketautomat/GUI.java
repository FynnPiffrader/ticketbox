package at.pif.ticketautomat;
import java.util.Scanner;
public class GUI {
    private Box box;

    public GUI(Box box) {
        this.box = box;
    }

    public void run(){
        System.out.println("Willkommen beim Ticketomat 3000");
        Scanner scan = new Scanner(System.in);
        while(true){
            System.out.println("1.Ticket einschieben, 2.Preis abrufen, 3.Geld einschieben");
            int auswahl = scan.nextInt();
            switch(auswahl){
                case 1:
                    box.getTicket();
                    break;
                case 2:
                    box.getPrice();
                    break;
                case 3:
                    break;
            }
        }
    }
}
