package at.pif.ticketautomat;

public class Main {
    public static void main(String[] args) {
        Ticket t1 = new Ticket();
        Drucker d1 = new Drucker(t1);
        Scanner s1 = new Scanner(t1);
        Geldausgabe g1 = new Geldausgabe();
        Box b1 = new Box(20, t1, d1, g1, s1);


        GUI gui = new GUI(b1);
        gui.run();


    }

}
