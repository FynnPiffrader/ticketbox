package at.pif.revision2;

import org.newdawn.slick.Game;

import java.util.Random;
import java.util.Scanner;

public class Wörterraten {
    public static void main(String[] args) {
        String words[] = {"Apfel", "Baum", "Banane", "Fortnite"};

        Random random = new Random();
        int randomNumber = random.nextInt(words.length);

        String wordToSearch = words[randomNumber];

        char[] wordToSearchArr = wordToSearch.toCharArray();
        char[] guessArr = new char[wordToSearchArr.length];

        for (int i = 0; i < wordToSearch.length(); i++) {
            guessArr[i] = '*';
        }

        //System.out.println(wordToSearchArr);
        System.out.println("Das gesuchte Wort lautet: ");
        System.out.println(guessArr);
        Scanner scanner = new Scanner(System.in);
        boolean GameStatus = true;
        while (GameStatus) {
            String guess = scanner.next();
            char searchChar = guess.toCharArray()[0];

            for (int i = 0; i < wordToSearch.length(); i++) {
                if (wordToSearchArr[i] == searchChar) {
                    guessArr[i] = searchChar;
                }
            }
            int count = 0;
            for (int i = 0; i < wordToSearch.length(); i++) {
                if (guessArr[i] != '*'){
                    count += 1;
                }
            }
            //System.out.println(count);

            if (count == wordToSearch.length()) {
                GameStatus = false;
                System.out.println("Congratulations! You guessed it!" +  " The word was: ");
            }


            System.out.println(guessArr);
        }
    }
}
